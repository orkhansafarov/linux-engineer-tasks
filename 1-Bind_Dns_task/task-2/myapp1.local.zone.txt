$TTL 3H
@       IN SOA  @ myapp1.local. (
                                        1       ; serial
                                        1D      ; refresh
                                        1H      ; retry
                                        1W      ; expire
                                        1H )    ; minimum
        NS      @
        A       127.0.0.1
        AAAA    ::1

host1 IN A 192.168.16.22
host2 IN A 192.168.16.23
