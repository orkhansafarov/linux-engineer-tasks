### 1. Install mariadb mariadb-server
I will use mariadb version 11.1 and CentOS 7 for this task.  
To Install mariadb we need to got ot website : https://mariadb.org/download/?t=mariadb  
at here with specific distro and mariadb version, we will be able to install mariadb by simply copy/pasting required commands.   

adding repostirory under yum.repos.d directory.  
```bash
echo "#MariaDB 11.1 CentOS repository list - created 2023-10-02 05:43 UTC
# https://mariadb.org/download/
[mariadb]
name = MariaDB
# rpm.mariadb.org is a dynamic mirror if your preferred mirror goes offline. See https://mariadb.org/mirrorbits/ for details.
# baseurl = https://rpm.mariadb.org/11.1/centos/$releasever/$basearch
baseurl = https://mirror.truenetwork.ru/mariadb/yum/11.1/centos/$releasever/$basearch
module_hotfixes = 1
# gpgkey = https://rpm.mariadb.org/RPM-GPG-KEY-MariaDB
gpgkey = https://mirror.truenetwork.ru/mariadb/yum/RPM-GPG-KEY-MariaDB
gpgcheck = 1
enabled=1
" > /etc/yum.repos.d/mariadb.repo
```

Check the availability of repoistory :  
```bash
[root@localhost ~]# yum repolist all | grep mariadb
mariadb/7/x86_64                    MariaDB                      enabled:     86
```

according to mariadb website, sometimes it is necessary to install epel repoistory first to proceed further on installation of mariadb. (pv dependency of galera):  
```bash 
[root@localhost ~]# yum install epel-release - y
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.yer.az
 * extras: mirror.yer.az
 * updates: mirror.yer.az
No package y available.
Resolving Dependencies
--> Running transaction check
---> Package epel-release.noarch 0:7-11 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

=====================================================================================================================================================================
 Package                                     Arch                                  Version                               Repository                             Size
=====================================================================================================================================================================
Installing:
 epel-release                                noarch                                7-11                                  extras                                 15 k

Transaction Summary
=====================================================================================================================================================================
```

installing mariadb-server and mariadb-client:   
```bash
yum install MariaDB-server MariaDB-client
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
epel/x86_64/metalink                                                                                                                          |  28 kB  00:00:00
 * base: mirror.yer.az
 * epel: mirror.logol.ru
 * extras: mirror.yer.az
 * updates: mirror.yer.az
epel                                                                                                                                          | 4.7 kB  00:00:00
(1/3): epel/x86_64/group_gz                                                                                                                   |  99 kB  00:00:00
Dependencies Resolved

=====================================================================================================================================================================
 Package                                         Arch                           Version                                        Repository                       Size
=====================================================================================================================================================================
Installing:
 MariaDB-client                                  x86_64                         11.1.2-1.el7.centos                            mariadb                          17 M
 MariaDB-compat                                  x86_64                         11.1.2-1.el7.centos                            mariadb                         2.2 M
     replacing  mariadb-libs.x86_64 1:5.5.68-1.el7
 MariaDB-server                                  x86_64                         11.1.2-1.el7.centos                            mariadb                          27 M
Installing for dependencies:
 MariaDB-client-compat                           noarch                         11.1.2-1.el7.centos                            mariadb                         7.9 k
 MariaDB-common                                  x86_64                         11.1.2-1.el7.centos                            mariadb                          82 k
 MariaDB-server-compat                           noarch                         11.1.2-1.el7.centos                            mariadb                         5.5 k
 boost-program-options                           x86_64                         1.53.0-28.el7                                  base                            156 k
 galera-4                                        x86_64                         26.4.14-1.el7.centos                           mariadb                         9.9 M
 libpmem                                         x86_64                         1.5.1-2.1.el7                                  base                             59 k
 lsof                                            x86_64                         4.87-6.el7                                     base                            331 k
 pcre2                                           x86_64                         10.23-2.el7                                    base                            201 k
 perl-Compress-Raw-Bzip2                         x86_64                         2.061-3.el7                                    base                             32 k
 perl-Compress-Raw-Zlib                          x86_64                         1:2.061-4.el7                                  base                             57 k
 perl-DBI                                        x86_64                         1.627-4.el7                                    base                            802 k
 perl-Data-Dumper                                x86_64                         2.145-3.el7                                    base                             47 k
 perl-IO-Compress                                noarch                         2.061-2.el7                                    base                            260 k
 perl-Net-Daemon                                 noarch                         0.48-5.el7                                     base                             51 k
 perl-PlRPC                                      noarch                         0.2020-14.el7                                  base                             36 k
 pv                                              x86_64                         1.4.6-1.el7                                    epel                             47 k
 rsync                                           x86_64                         3.1.2-12.el7_9                                 updates                         408 k
 socat                                           x86_64                         1.7.3.2-2.el7                                  base                            290 k

Transaction Summary
=====================================================================================================================================================================
```

query from rpm for mariadb:  
```bash
[root@localhost ~]# rpm -qa | grep -i mariadb*
MariaDB-common-11.1.2-1.el7.centos.x86_64
MariaDB-client-compat-11.1.2-1.el7.centos.noarch
MariaDB-server-compat-11.1.2-1.el7.centos.noarch
MariaDB-compat-11.1.2-1.el7.centos.x86_64
MariaDB-client-11.1.2-1.el7.centos.x86_64
MariaDB-server-11.1.2-1.el7.centos.x86_64
```

starting mysql and mysql secure installation:  
```
[root@localhost ~]# systemctl start mariadb
[root@localhost ~]#
[root@localhost ~]#
[root@localhost ~]# mariadb-secure-installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] n
 ... skipping.

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] n
 ... skipping.

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[root@localhost ~]#
```


### 2. Install phpMyAdmin and configure

We need to install php php-mysql httpd and phpMyAdmin properly for making this setup work   

To stary with, first is php and php-mysql:  
```bash
[root@localhost ~]# yum install php php-mysql
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.yer.az
 * epel: mirror.logol.ru
 * extras: mirror.yer.az
 * updates: mirror.yer.az
Resolving Dependencies
Dependencies Resolved

=====================================================================================================================================================================
 Package                                Arch                              Version                                           Repository                          Size
=====================================================================================================================================================================
Installing:
 php                                    x86_64                            5.4.16-48.el7                                     base                               1.4 M
 php-mysql                              x86_64                            5.4.16-48.el7                                     base                               102 k
Installing for dependencies:
 apr                                    x86_64                            1.4.8-7.el7                                       base                               104 k
 apr-util                               x86_64                            1.5.2-6.el7_9.1                                   updates                             92 k
 httpd                                  x86_64                            2.4.6-99.el7.centos.1                             updates                            2.7 M
 httpd-tools                            x86_64                            2.4.6-99.el7.centos.1                             updates                             94 k
 libzip                                 x86_64                            0.10.1-8.el7                                      base                                48 k
 mailcap                                noarch                            2.1.41-2.el7                                      base                                31 k
 php-cli                                x86_64                            5.4.16-48.el7                                     base                               2.7 M
 php-common                             x86_64                            5.4.16-48.el7                                     base                               565 k
 php-pdo                                x86_64                            5.4.16-48.el7                                     base                                99 k

Transaction Summary
=====================================================================================================================================================================
```

Then installing httpd:   
```bash
[root@localhost ~]# yum install httpd
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.yer.az
 * epel: mirror.logol.ru
 * extras: mirror.yer.az
 * updates: mirror.yer.az
Package httpd-2.4.6-99.el7.centos.1.x86_64 already installed and latest version
Nothing to do
```

Next and lats step is phpMyAdmin. To do this we will need epel repository. Since we already installed and configured EPEL, I'm skipping this step.   
```bash
[root@localhost ~]# yum install phpmyadmin
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.yer.az
 * epel: mirror.logol.ru
 * extras: mirror.yer.az
 * updates: mirror.yer.az
Resolving Dependencies
Dependencies Resolved

=====================================================================================================================================================================
 Package                                            Arch                          Version                                       Repository                      Size
=====================================================================================================================================================================
Installing:
 phpMyAdmin                                         noarch                        4.4.15.10-6.el7                               epel                           4.7 M
Installing for dependencies:
 compat-libtidy                                     x86_64                        0.99.0-37.20091203.el7                        epel                           133 k
 dejavu-fonts-common                                noarch                        2.33-6.el7                                    base                            64 k
 dejavu-sans-fonts                                  noarch                        2.33-6.el7                                    base                           1.4 M
 fontpackages-filesystem                            noarch                        1.44-8.el7                                    base                           9.9 k
 libX11                                             x86_64                        1.6.7-4.el7_9                                 updates                        607 k
 libX11-common                                      noarch                        1.6.7-4.el7_9                                 updates                        164 k
 libXau                                             x86_64                        1.0.8-2.1.el7                                 base                            29 k
 libXpm                                             x86_64                        3.5.12-2.el7_9                                updates                         56 k
 libjpeg-turbo                                      x86_64                        1.2.90-8.el7                                  base                           135 k
 libxcb                                             x86_64                        1.13-1.el7                                    base                           214 k
 php-bcmath                                         x86_64                        5.4.16-48.el7                                 base                            58 k
 php-fedora-autoloader                              noarch                        1.0.1-2.el7                                   epel                            11 k
 php-gd                                             x86_64                        5.4.16-48.el7                                 base                           128 k
 php-mbstring                                       x86_64                        5.4.16-48.el7                                 base                           506 k
 php-php-gettext                                    noarch                        1.0.12-1.el7                                  epel                            23 k
 php-process                                        x86_64                        5.4.16-48.el7                                 base                            56 k
 php-tcpdf                                          noarch                        6.2.26-1.el7                                  epel                           2.1 M
 php-tcpdf-dejavu-sans-fonts                        noarch                        6.2.26-1.el7                                  epel                           257 k
 php-tidy                                           x86_64                        5.4.16-9.el7                                  epel                            23 k
 php-xml                                            x86_64                        5.4.16-48.el7                                 base                           126 k
 t1lib                                              x86_64                        5.1.2-14.el7                                  base                           166 k

Transaction Summary
=====================================================================================================================================================================
```

Now its time to configure!. To conigure phpmyadmin we need to go /etc/httpd/conf.d/phpMyAdmin.conf file and edit it like this :  

We can see that there are 2 directories. phpMyAdmin and phpMyAdmin/setup which is essential to us. We need to add our host IP as Require ip sections (In this case my additions are : Require ip 192.168.16.1 and Allow from 192.168.16.1. I did this to both 2 direcotry sections. my own IP address is 192.168.16.128). :  
```bash
<Directory /usr/share/phpMyAdmin/>
   AddDefaultCharset UTF-8

   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       Require ip 127.0.0.1
       Require ip ::1
       Require ip 192.168.16.1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
     Allow from 192.168.16.1
   </IfModule>
</Directory>

<Directory /usr/share/phpMyAdmin/setup/>
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       Require ip 127.0.0.1
       Require ip ::1
       Require ip 192.168.16.1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
     Allow from 192.168.16.1
   </IfModule>
</Directory>

```

[/etc/httpd/conf.d/phpMyAdmin.conf complete file](/3-MariaDB_task/files_and_screenshots/phpMyAdmin.conf)

After that we have to add following line at the end of the /etc/httpd/conf/httpd.conf file:  
```bash 
<IfModule dir_module>
    DirectoryIndex index.html index.php
</IfModule>
```

[/etc/httpd/conf/httpd.conf complete file](/3-MariaDB_task/files_and_screenshots/httpd.conf)

After it we are going to restart httpd and see the status either:  
```bash
[root@localhost ~]# systemctl restart httpd
[root@localhost ~]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2023-10-02 10:06:57 +04; 5s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 9295 (httpd)
   Status: "Processing requests..."
   CGroup: /system.slice/httpd.service
           ├─9295 /usr/sbin/httpd -DFOREGROUND
           ├─9296 /usr/sbin/httpd -DFOREGROUND
           ├─9297 /usr/sbin/httpd -DFOREGROUND
           ├─9298 /usr/sbin/httpd -DFOREGROUND
           ├─9299 /usr/sbin/httpd -DFOREGROUND
           └─9300 /usr/sbin/httpd -DFOREGROUND

Oct 02 10:06:57 localhost.localdomain systemd[1]: Starting The Apache HTTP Server...
Oct 02 10:06:57 localhost.localdomain httpd[9295]: AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using localhost.localdomain. Se...his message
Oct 02 10:06:57 localhost.localdomain systemd[1]: Started The Apache HTTP Server.
Hint: Some lines were ellipsized, use -l to show in full.
[root@localhost ~]#
```

Here is screenshot of phpMyAdmin on web-browser: 

[phpMyAdmin Screenshot](/3-MariaDB_task/files_and_screenshots/pypmyadmin.PNG)

### 3. Create Database table with
   ##### 3.1 ID uniq , auto increment ,int
   ##### 3.2 Any 4 column that is 20 characters long type string
   ##### 3.3 Column which automatically writes the current date-time

I created a database garage, which represents a mall garage with registered members. In this case members will be in the "members" table    
For making ID unique, I added UNIQUE constraint while in table creation    
Next 4 columns are string which describes name, surname, carmodel, status which is for managing members of garage of mall.    
for automatic current date-time column I used DATETIME datatype and added DEFAULT and CURRENT_TIMESTAMP constrains. DEFAULT CURRENT_TIMESTAMP writes default date-time value which will automatically the current date-time. So if we do not add anything to that column it will automatically be filled.     

```bash
[root@localhost ~]# mysql -u root -p
mysql: Deprecated program name. It will be removed in a future release, use '/usr/bin/mariadb' instead
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 11.1.2-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database garage;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> use garage;
Database changed
MariaDB [garage]> create table members (
    -> ID INT AUTO_INCREMENT UNIQUE,
    -> NAME VARCHAR(20) NOT NULL,
    -> SURNAME VARCHAR(20) NOT NULL,
    -> CAR_NAME VARCHAR(20) NOT NULL,
    -> STATUS VARCHAR(10) NOT NULL,
    -> DATE DATETIME DEFAULT CURRENT_TIMESTAMP
    -> );
Query OK, 0 rows affected (0.012 sec)

MariaDB [garage]> show columns from members
    -> ;
+----------+-------------+------+-----+---------------------+----------------+
| Field    | Type        | Null | Key | Default             | Extra          |
+----------+-------------+------+-----+---------------------+----------------+
| ID       | int(11)     | NO   | PRI | NULL                | auto_increment |
| NAME     | varchar(20) | NO   |     | NULL                |                |
| SURNAME  | varchar(20) | NO   |     | NULL                |                |
| CAR_NAME | varchar(20) | NO   |     | NULL                |                |
| STATUS   | varchar(10) | NO   |     | NULL                |                |
| DATE     | datetime    | YES  |     | current_timestamp() |                |
+----------+-------------+------+-----+---------------------+----------------+
6 rows in set (0.003 sec)
```

We are going to check the table by adding a record to it.     
Record :   Simon Williams with an mercedes is in the garage.    

```bash
MariaDB [garage]> insert into members (
    -> ID , NAME , SURNAME , CAR_NAME , STATUS )
    -> values (1 , "Simon" , "Williams" , "Mercedes" , "in");
Query OK, 1 row affected (0.003 sec)

MariaDB [garage]> select * from members
    -> ;
+----+-------+----------+----------+--------+---------------------+
| ID | NAME  | SURNAME  | CAR_NAME | STATUS | DATE                |
+----+-------+----------+----------+--------+---------------------+
|  1 | Simon | Williams | Mercedes | in     | 2023-10-02 11:16:18 |
+----+-------+----------+----------+--------+---------------------+
1 row in set (0.001 sec)

MariaDB [garage]>
```
Another record. but this time without giving ID value.       
Record :   James Thompson with an BMW is not in the garage.     

```bash
MariaDB [garage]> insert into members
    -> (NAME , SURNAME , CAR_NAME , STATUS )
    -> values ("James" , "Thompson" , "BMW" , "out");
Query OK, 1 row affected (0.002 sec)

MariaDB [garage]> select * from members
    -> ;
+----+-------+----------+----------+--------+---------------------+
| ID | NAME  | SURNAME  | CAR_NAME | STATUS | DATE                |
+----+-------+----------+----------+--------+---------------------+
|  1 | Simon | Williams | Mercedes | in     | 2023-10-02 11:16:18 |
|  2 | James | Thompson | BMW      | out    | 2023-10-02 11:19:21 |
+----+-------+----------+----------+--------+---------------------+
2 rows in set (0.001 sec)

MariaDB [garage]>
```
   

### 4. Send result screen from phpMyAdmin

Here the same results from phpmyadmin UI :    
[PhpMyAdmin web application results](/3-MariaDB_task/files_and_screenshots/phpmyadmin_garage_members_table.PNG)

### 5. Create 3 users :
   ##### 5.1 Grant All permision to one
   ##### 5.2 Grant only view permision to second, and connect only from localhost
   ##### 5.3 Grant select update delete permisions to third
   ##### 5.4 Allow the same user to connect from 2 different IPs, and give those users different CRUD permissions. Connect the check which role will be applied to the user.

My users will be in line according to questions : Orkhan , Sarah , John , (Simon with 2 different IPs)   
  
Orkhan User:    
```bash
MariaDB [(none)]> CREATE USER orkhan IDENTIFIED BY '123';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> GRANT all on *.* to orkhan;
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> show grants for orkhan;
+----------------------------------------------------------------------------------------------------------------+
| Grants for orkhan@%                                                                                            |
+----------------------------------------------------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO `orkhan`@`%` IDENTIFIED BY PASSWORD '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257' |
+----------------------------------------------------------------------------------------------------------------+
1 row in set (0.000 sec)

MariaDB [(none)]>
```

Sarah User:   
```bash
MariaDB [(none)]> create user 'sarah'@'localhost' identified by '123';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> grant select on *.* to 'sarah'@'localhost';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> show grants for 'sarah'@'localhost';
+---------------------------------------------------------------------------------------------------------------+
| Grants for sarah@localhost                                                                                    |
+---------------------------------------------------------------------------------------------------------------+
| GRANT SELECT ON *.* TO `sarah`@`localhost` IDENTIFIED BY PASSWORD '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257' |
+---------------------------------------------------------------------------------------------------------------+
1 row in set (0.000 sec)
```

I only demonstrated "\*.\*" in the name of databases. But in John user I will give those privileges to specific table, which is garage.members.   
John User:   
```bash
MariaDB [(none)]> create user john identified by '123';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> grant select, update, delete on garage.members to john;
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> show grants for john
    -> ;
+-----------------------------------------------------------------------------------------------------+
| Grants for john@%                                                                                   |
+-----------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `john`@`%` IDENTIFIED BY PASSWORD '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257' |
| GRANT SELECT, UPDATE, DELETE ON `garage`.`members` TO `john`@`%`                                    |
+-----------------------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)
```
    
          
Simon user will be able to select privilege on 192.168.16.111 and all privileges on 192.168.16.222 for garage.members table.  
   
Simon User (192.168.16.111):  
```bash
MariaDB [(none)]> create user 'simon'@'192.168.16.111' identified by '123';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> grant select on garage.members to 'simon'@'192.168.16.111';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> show grants for 'simon'@'192.168.16.111';
+-------------------------------------------------------------------------------------------------------------------+
| Grants for simon@192.168.16.111                                                                                   |
+-------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `simon`@`192.168.16.111` IDENTIFIED BY PASSWORD '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257' |
| GRANT SELECT ON `garage`.`members` TO `simon`@`192.168.16.111`                                                    |
+-------------------------------------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)
```
   
Simon User (192.168.16.222):  
```bash
MariaDB [(none)]> create user 'simon'@'192.168.16.222' identified by '123';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> grant all on garage.members to 'simon'@'192.168.16.222';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> show grants for 'simon'@'192.168.16.222';
+-------------------------------------------------------------------------------------------------------------------+
| Grants for simon@192.168.16.222                                                                                   |
+-------------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `simon`@`192.168.16.222` IDENTIFIED BY PASSWORD '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257' |
| GRANT ALL PRIVILEGES ON `garage`.`members` TO `simon`@`192.168.16.222`                                            |
+-------------------------------------------------------------------------------------------------------------------+
2 rows in set (0.000 sec)

MariaDB [(none)]>
```
