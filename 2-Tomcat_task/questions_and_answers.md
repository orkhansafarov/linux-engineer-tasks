## 1. Download Java and Tomcat files
First I created tomcat user and gave him some sudo commands     
```bash
useradd tomcat  
passwd tomcat   

tomcat   ALL=(ALL)      /usr/bin/systemctl, /usr/bin/netstat, /usr/bin/firewall-cmd      
```  
[user add](/2-Tomcat_task/screenshots/tomcat_user_creation.PNG)    
[sudo priviliege](/2-Tomcat_task/screenshots/tomcat_sudo.PNG)    
I used jdk 11.0.2 and compatible tomcat which is 10.    
```bash 
wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz  
wget https://dlcdn.apache.org/tomcat/tomcat-10/v10.1.13/bin/apache-tomcat-10.1.13.tar.gz  
```   
[installation screenshot](/2-Tomcat_task/screenshots/tomcat_installation.PNG)    
[extraction and move](/2-Tomcat_task/screenshots/tomcat_java_extract_move.PNG)    
## 2. Create TOMCAT_BASE directory, and directory for java  
I created folder, added java and tomcat. And changed ownership of /app folder     
```bash  
mkdir /app/java_version -p  
mkdir /app/tomcat/ -p   
 
chown -R tomcat:tomcat /app     
```  
[folder creation and ownership](/2-Tomcat_task/screenshots/folder_creation.PNG)    
## 3. Download any .war java project and deploy it as single instance .    
```bash  
wget https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war  
cp sample.war /app/tomcat/apache-tomcat-10.1.13/webapp/  
```  
[sample application installation and deployment under .../webapp](/2-Tomcat_task/screenshots/sample_war_installation_and_deployment.PNG)   
defining variables on .bash_profile in tomcat user   
```bash  
vim .bash_profile   
JAVA_HOME=/app/java_version/jdk-11.0.2/   
PATH=$PATH:$JAVA_HOME/bin   
export JAVA_HOME PATH   
```
[defining variable of java](/2-Tomcat_task/screenshots/java_variable_define.PNG)     

Next I configured firewall    
[firewall configuration](/2-Tomcat_task/screenshots/firewall_single.PNG)      

Then I started tomcat with built-in script  
```bash  
cd /app/tomcat/apache-tomcat-10.1.1/bin  
./catalina.sh start  
```
Result will be in localhost:8080/sample   
screenshot : [website](/2-Tomcat_task/screenshots/sample_website.PNG)  

## 4. Make a tomcat consisting of at least 2 instance  

First I made little changes to make my environment clear to you.     
I changed /app/tomcat folder to /app/instances for indicating the instances  
I copied 1 tomcat app folder under /app directory. It will serve for CATALINA_HOME variable   
Then I changed old /app/tomcat/apache-tomcat-10.1.13 folder to payment-app which the sample.war file still inside of it     
then I duplicated the payment-app folder in same directory and renamed it billing-app.     
Now I have two tomcat folders (payment-app and billing-app) inside of instances    
Folder Hiearch is now looks like this [/app folder hierarchy](/2-Tomcat_task/screenshots/tree_folder_new.PNG)    

to make big difference I made changes on each sample index.html file in both *-app folders  
[billing-app index.html](/2-Tomcat_task/screenshots/billing_app.PNG)  
[payment-app index.html](/2-Tomcat_task/screenshots/payment_app.PNG)  

Now I created tomcat-instance.sh under /app folder and giving variables and scripts necessary to start/stop tomcat  
screenshot :[tomcat-instance.sh](/2-Tomcat_task/screenshots/tomcat_instance_script.PNG)  
script file in gitlab :[tomcat-instance.sh](/2-Tomcat_task/files/tomcat-instance.sh)  
[now with all completed. tree look like this](/2-Tomcat_task/screenshots/tree_folder_last.PNG)  

Tomcat cant run two instances in one port. I will change the ports to 9090 and 9443 in  server.xml file of billing-app :    
[change the ports to 9090 and 9443](/2-Tomcat_task/screenshots/server_xml.PNG)       

then firewall needs to recognize 9090 port :    
[firewall configured](/2-Tomcat_task/screenshots/firewall_9090.PNG)     

A good alias can make our code clear:  
[aliasing tomcat-instance.sh](/2-Tomcat_task/screenshots/aliasing_script.PNG)   

After all of that we can start two tomcats with these command :  
```bash  
tomcat-instance start billing-app  
tomcat-instance start payment-app  
```
[Starting both tomcats and netstat](/2-Tomcat_task/screenshots/multi_instance_start.PNG)    

both websites :  
[web billing-app](/2-Tomcat_task/screenshots/web_billing.PNG)  
[web payment-app](/2-Tomcat_task/screenshots/web_payment.PNG)  
