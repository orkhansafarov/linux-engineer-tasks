#!/bin/bash
# script will take 2 arguments at the same time.
# syntax : tomcat-instance [start/stop] [instanceName]

if [[ $# -lt 2 ]]
then
        echo "Not enough arguments! Command is not executed."
        echo "Correct syntax : tomcat-instance [start/stop] [instanceName]"
else


# arguments for script
operation=$1
instance_name=$2

# Variables for tomcat and script
TOMCAT_BASE=/app
INSTANCE_BASE=$TOMCAT_BASE/instances
CATALINA_HOME=$TOMCAT_BASE/tomcat-10.1.13
CATALINA_BASE=$INSTANCE_BASE/$instance_name
export CATALINA_BASE CATALINA_HOME

# script will take 2 arguments at the same time.
# syntax : tomcat-instance [start/stop] [instanceName]

        if [[ $1 == "start" ]]
        then
                $CATALINA_HOME/bin/startup.sh
        elif [[ $1 == "stop" ]]
        then
                $CATALINA_HOME/bin/shutdown.sh
        else
                echo "incorrect argument! Command is not executed."
                echo "correct syntax for argument is : \"start\" or \"stop\""
        fi


fi
